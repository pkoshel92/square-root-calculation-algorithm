package src;

public class SquareRootCalculationAlgorithm {
public static double squareRoot (double n) {

    double sqrRoot = 0;
    double y = 1;
    double x = n;
    double tolerance = 1e-4;

    if (n<0) {
        throw new IllegalArgumentException();
    } else if (n>0) {
        while (x != y*y) {
            sqrRoot = (n/y + y)/2;
            y = sqrRoot;
            if (Math.abs(x - y * y) < tolerance) {
                break;
            }
        }
    } else {
        return n;
    }
    return Math.floor(sqrRoot);
}
}
